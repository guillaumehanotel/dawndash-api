<?php

namespace App\Http\Middleware;

use App\Exceptions\InvalidTokenException;
use App\Services\UserService;
use Closure;
use Illuminate\Http\Request;

class CheckGoogleTokenValidity
{
    public UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function handle(Request $request, Closure $next)
    {
        try {
            $this->userService->getGoogleProfileFromRequest($request);
            return $next($request);
        } catch (InvalidTokenException $exception) {
            return response($exception->getMessage(), 401);
        }
    }
}
