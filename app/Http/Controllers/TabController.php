<?php

namespace App\Http\Controllers;

use App\Models\Tab;
use App\Models\User;
use App\Transformers\TabTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Resource\Collection;

class TabController extends Controller
{

    public function getTabsByUser(Request $request, User $user)
    {
        try {
            $tabs = Tab::whereUserId($user->id)->get();
            $transformedTabs = $this->fractal->createData(new Collection($tabs, $this->tabTransformer));
            return $transformedTabs->toArray();
        } catch (Exception $exception) {
            return response($exception->getMessage(), 500);
        }
    }

    public function store(Request $request, User $user)
    {
        try {
            $body = $request->all();
            $tab = new Tab();
            $tab->title = $body['title'];
            $tab->order_position = $body['orderPosition'];
            $tab->user()->associate($user);
            $tab->save();

            return response((new TabTransformer())->transform($tab), 201);
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    public function update(Request $request, Tab $tab): Response
    {
        $body = $request->all();
        $tab->title = $body['title'];
        $tab->save();
        return response()->noContent(200);
    }

    public function delete(Tab $tab): Response
    {
        $tab->delete();
        return response()->noContent();
    }
}
