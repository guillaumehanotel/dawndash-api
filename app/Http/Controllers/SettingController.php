<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Exception;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function update(Request $request, Setting $setting)
    {
        try {
            $body = $request->all();
            $setting->background_image_url = $body['backgroundImageUrl'];
            $setting->bookmark_width = $body['bookmarkWidth'];
            $setting->show_bookmark_title = $body['showBookmarkTitle'];
            $setting->open_in_new_tab = $body['openInNewTab'];
            $setting->save();
            return response()->noContent(200);
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }
    }
}
