<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use League\Fractal\Resource\Item;

class UserController extends Controller
{
    public function getOrCreateUser(Request $request)
    {
        $googleProfile = $this->userService->getGoogleProfileFromRequest($request);
        $user = $this->userService->getUserByGoogleUid($googleProfile->googleUid);
        if ($user == null) {
            $user = $this->userService->createUserFromGoogleProfile($googleProfile);
            $this->userService->createDefaultTabAndBookmarkForUser($user);
            $user = $this->userService->getUserByGoogleUid($googleProfile->googleUid);
        }
        return $this->fractal->createData(new Item($user, $this->userTransformer));
    }

    public function index()
    {
        return User::all();
    }
}
