<?php

namespace App\Http\Controllers;

use App\Models\Bookmark;
use App\Models\Tab;
use App\Models\User;
use App\Services\ImportBookmarkService;
use App\Transformers\BookmarkTransformer;
use App\Transformers\TabTransformer;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BookmarkController extends Controller
{
    public function store(Request $request)
    {
        try {
            $tab = Tab::find($request->input('tabId'));
            $bookmark = new Bookmark();
            $bookmark->title = getSiteTitleFromUrl($request->input('url'));
            $bookmark->is_link = $request->input('isLink');
            $bookmark->order_position = $request->input('orderPosition');
            $bookmark->parent_bookmark_id = $request->input('parentBookmarkId');
            $bookmark->url = $request->input('url');

            if ($bookmark->is_link) {
                // Issue a reset API call:
                Http::get('http://free.pagepeeker.com/v2/thumbs_ready.php?size=x&refresh=1&url=' . $bookmark->url);
                $bookmark->thumbnail_url = 'http://free.pagepeeker.com/v2/thumbs.php?size=x&url=' . $bookmark->url;
            }

            $bookmark->tab()->associate($tab);
            $bookmark->save();
            return response((new BookmarkTransformer())->transform($bookmark), 201);
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    public function update(Request $request, Bookmark $bookmark): Response
    {
        $body = $request->all();
        $bookmark->title = $body['title'];
        $bookmark->url = $body['url'];
        $bookmark->thumbnail_url = $body['thumbnailUrl'];
        $bookmark->is_link = $body['isLink'];
        $bookmark->order_position = $body['orderPosition'];
        $bookmark->save();
        return response()->noContent(200);
    }

    public function bulkUpdate(Request $request): Response
    {
        $body = $request->all();
        foreach ($body as $bookmarkArray) {
            $bookmark = Bookmark::find($bookmarkArray['id']);
            $bookmark->order_position = $bookmarkArray['orderPosition'];
            $bookmark->save();
        }
        return response()->noContent(200);
    }

    public function delete(Bookmark $bookmark): Response
    {
        \Log::info(Bookmark::all()->map(fn($bf) => $bf->id));
        $bookmark->delete();
        return response()->noContent();
    }

    public function importJsonChromeBookmarks(Request $request): Response
    {
        $now = date("Ymd_His");
        $path = $request->file('file')->storeAs(
            'bookmarks',
            $now . "-bookmarks-" . Str::random(9) . ".json"
        );
        $tab = new Tab();
        $tab->title = "Bookmarks Bar";
        $max = \DB::table('tabs')->max('order_position');
        $tab->order_position = $max + 1;
        $tab->save();
        (new ImportBookmarkService($tab))->importChromeBookmarkJsonFile($path);
        return response((new TabTransformer())->transform($tab), 201);
    }
}
