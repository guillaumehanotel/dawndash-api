<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Transformers\CustomSerializer;
use App\Transformers\TabTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal\Manager;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public UserService $userService;
    public TabTransformer $tabTransformer;
    public UserTransformer $userTransformer;
    public Manager $fractal;

    public function __construct(
        UserService $userService,
        TabTransformer $tabTransformer,
        UserTransformer $userTransformer,
        Manager $fractal
    ) {
        $this->userService = $userService;
        $this->tabTransformer = $tabTransformer;
        $this->userTransformer = $userTransformer;
        $this->fractal = $fractal;
        $this->fractal->setSerializer(new CustomSerializer());
    }
}
