<?php


namespace App\Transformers;

use App\Models\Tab;
use League\Fractal\TransformerAbstract;

class TabTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'bookmarks'
    ];

    public function transform(Tab $tab)
    {
        return [
            'id' => $tab->id,
            'title' => $tab->title,
            'orderPosition' => $tab->order_position,
            'userId' => $tab->user->id,
        ];
    }

    public function includeBookmarks(Tab $tab)
    {
        return $this->collection($tab->bookmarks, new BookmarkTransformer);
    }
}
