<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'setting'
    ];

    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'email' => $user->email,
            'googleUid' => $user->google_uid,
            'fullname' => $user->fullname,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'avatarUrl' => $user->avatar_url,
        ];
    }

    public function includeSetting(User $user)
    {
        return $this->item($user->setting, new SettingTransformer);
    }
}
