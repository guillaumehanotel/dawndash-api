<?php

namespace App\Transformers;

use App\Models\Bookmark;
use League\Fractal\TransformerAbstract;

class BookmarkTransformer extends TransformerAbstract
{
    public function transform(Bookmark $bookmark): array
    {
        return [
            'id' => $bookmark->id,
            'isLink' => $bookmark->is_link,
            'orderPosition' => $bookmark->order_position,
            'parentBookmarkId' => $bookmark->parent_bookmark_id,
            'thumbnailUrl' => $bookmark->thumbnail_url,
            'title' => $bookmark->title,
            'url' => $bookmark->url,
            'tabId' => $bookmark->tab_id
        ];
    }
}
