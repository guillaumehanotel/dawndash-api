<?php

namespace App\Transformers;

use App\Models\Setting;
use League\Fractal\TransformerAbstract;

class SettingTransformer extends TransformerAbstract
{
    public function transform(Setting $setting)
    {
        return [
            'id' => $setting->id,
            'backgroundImageUrl' => $setting->background_image_url,
            'bookmarkWidth' => $setting->bookmark_width,
            'showBookmarkTitle' => $setting->show_bookmark_title,
            'openInNewTab' => $setting->open_in_new_tab,
        ];
    }
}
