<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Tab
 *
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $title
 * @property int $order_position
 * @property int $user_id
 * @property-read Collection|Bookmark[] $bookmarks
 * @property-read int|null $bookmarks_count
 * @property-read User $user
 * @method static Builder|Tab newModelQuery()
 * @method static Builder|Tab newQuery()
 * @method static Builder|Tab query()
 * @method static Builder|Tab whereCreatedAt($value)
 * @method static Builder|Tab whereId($value)
 * @method static Builder|Tab whereOrderPosition($value)
 * @method static Builder|Tab whereTitle($value)
 * @method static Builder|Tab whereUpdatedAt($value)
 * @method static Builder|Tab whereUserId($value)
 * @mixin Eloquent
 */
class Tab extends Model
{
    use HasFactory;

    protected $with = ['bookmarks'];

    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
