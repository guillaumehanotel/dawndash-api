<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Bookmark
 *
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $is_link
 * @property int $order_position
 * @property string $url
 * @property string $thumbnail_url
 * @property string $title
 * @property int|null $parent_bookmark_id
 * @property int $tab_id
 * @property int $user_id
 * @property-read Tab $tab
 * @property-read User $user
 * @method static Builder|Bookmark newModelQuery()
 * @method static Builder|Bookmark newQuery()
 * @method static Builder|Bookmark query()
 * @method static Builder|Bookmark whereCreatedAt($value)
 * @method static Builder|Bookmark whereId($value)
 * @method static Builder|Bookmark whereIsLink($value)
 * @method static Builder|Bookmark whereOrderPosition($value)
 * @method static Builder|Bookmark whereParentBookmarkId($value)
 * @method static Builder|Bookmark whereTabId($value)
 * @method static Builder|Bookmark whereText($value)
 * @method static Builder|Bookmark whereThumbnailUrl($value)
 * @method static Builder|Bookmark whereUpdatedAt($value)
 * @method static Builder|Bookmark whereUrl($value)
 * @method static Builder|Bookmark whereUserId($value)
 * @method static Builder|Bookmark whereTitle($value)
 * @mixin Eloquent
 */
class Bookmark extends Model
{
    use HasFactory;

    public function tab()
    {
        return $this->belongsTo(Tab::class);
    }
}
