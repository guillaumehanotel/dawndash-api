<?php

namespace App\Models;

class GoogleProfile
{
    public string $googleUid;
    public string $email;
    public string $fullname;
    public string $firstname;
    public string $lastname;
    public string $avatarUrl;

    public function __construct(array $payload)
    {
        $this->googleUid = $payload['sub'];
        $this->email = $payload['email'];
        $this->fullname = $payload['name'];
        $this->firstname = $payload['given_name'];
        $this->lastname = $payload['family_name'];
        $this->avatarUrl = $payload['picture'];
    }

    public function __toString()
    {
        return implode("\n", (array)$this);
    }
}
