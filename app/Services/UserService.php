<?php

namespace App\Services;

use App\Exceptions\InvalidTokenException;
use App\Models\Bookmark;
use App\Models\GoogleProfile;
use App\Models\Setting;
use App\Models\Tab;
use App\Models\User;
use Google\Client;
use Illuminate\Http\Request;

class UserService
{

    private Client $client;

    public function __construct()
    {
        $this->client = new Client(['client_id' => env('GOOGLE_CLIENT_ID')]);
    }

    public function getUserFromRequest(Request $request)
    {
        $googleProfile = $this->getGoogleProfileFromRequest($request);
        if ($googleProfile) {
            return $this->getUserByGoogleUid($googleProfile->googleUid);
        }
        return null;
    }

    public function getUserByGoogleUid(string $googleUid)
    {
        return User::whereGoogleUid($googleUid)->first();
    }

    public function createUserFromGoogleProfile(GoogleProfile $googleProfile): User
    {
        $user = new User();
        $user->firstname = $googleProfile->firstname;
        $user->lastname = $googleProfile->lastname;
        $user->fullname = $googleProfile->fullname;
        $user->email = $googleProfile->email;
        $user->avatar_url = $googleProfile->avatarUrl;
        $user->google_uid = $googleProfile->googleUid;
        $setting = Setting::create();
        $user->setting_id = $setting->id;
        $user->save();
        return $user;
    }

    public function createDefaultTabAndBookmarkForUser(User $user): void
    {
        $tab = new Tab();
        $tab->order_position = 1;
        $tab->title = "Default";
        $tab->user()->associate($user);
        $tab->save();

        $bookmark = new Bookmark();
        $bookmark->is_link = true;
        $bookmark->order_position = 1;
        $bookmark->parent_bookmark_id = null;
        $bookmark->title = "Google";
        $bookmark->url = "https://www.google.fr/";
        $bookmark->thumbnail_url = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_160x56dp.png";
        $bookmark->tab()->associate($tab);
        $bookmark->save();
    }

    /**
     * @param Request $request
     * @return GoogleProfile|null
     * @throws InvalidTokenException
     */
    public function getGoogleProfileFromRequest(Request $request): ?GoogleProfile
    {
        $tokenId = $this->getTokenFromRequest($request);
        if ($tokenId) {
            $payload = $this->client->verifyIdToken($tokenId);
            if ($payload) {
                return new GoogleProfile($payload);
            } else {
                throw new InvalidTokenException("No 'Authorization' header provided or Invalid Token");
            }
        } else {
            throw new InvalidTokenException("No 'Authorization' header provided or Invalid Token");
        }
    }

    /**
     * @param Request $request
     * @return mixed|string|null
     */
    private function getTokenFromRequest(Request $request): ?string
    {
        $autorizationHeader = $request->header('Authorization');
        if ($autorizationHeader) {
            return explode(" ", $autorizationHeader)[1];
        }
        return null;
    }
}
