<?php

namespace App\Services;

use App\Models\Bookmark;
use App\Models\Tab;
use Illuminate\Database\Eloquent\Model;

class ImportBookmarkService
{
    private Tab $tab;

    public function __construct(Tab $tab)
    {
        $this->tab = $tab;
    }

    public function importChromeBookmarkJsonFile(string $filePath): array
    {
        $jsonString = file_get_contents($filePath);
        $data = json_decode($jsonString, true);
        $bookmarksNodes = $data['roots']['bookmark_bar']['children'];
        return $this->parseBookmarks($bookmarksNodes);
    }

    private function parseBookmarks($bookmarksNodes, $parent = null, $depth = 0): array
    {
        $bookmarks = [];
        foreach ($bookmarksNodes as $bookmarkNode) {
            $bookmark = self::storeBookmark($bookmarkNode, $parent);
            array_push($bookmarks, $bookmark);
            if ($bookmarkNode['type'] == 'folder') {
                array_push($bookmarks, ...$this->parseBookmarks($bookmarkNode['children'], $bookmark, $depth + 1));
            }
        }
        return $bookmarks;
    }

    private function storeBookmark($bookmarkNode, Bookmark $parent = null): Bookmark
    {
        $bookmark = new Bookmark();
        $bookmark->parent_bookmark_id = $parent->id;
        $bookmark->id = $bookmarkNode['id'];
        $bookmark->title = $bookmarkNode['name'];
        $bookmark->url = $bookmarkNode['url'] ?? null;
        $bookmark->is_link = $bookmarkNode['type'] == 'url';
        $bookmark->tab()->associate($this->tab);
        $bookmark->save();
        return $bookmark;
    }
}
