<?php

/**
 * @param string $url URL of a website
 * @return string The title of the website
 */
function getSiteTitleFromUrl(string $url): string
{
    $doc = new DOMDocument();
    @$doc->loadHTMLFile($url);
    $xpath = new DOMXPath($doc);
    return $xpath->query('//title')->item(0)->nodeValue;
}
