<?php

use App\Http\Middleware\CheckGoogleTokenValidity;
use App\Models\Bookmark;
use App\Models\Tab;
use App\Models\User;
use function Pest\Laravel\get;
use function Pest\Laravel\post;
use function Pest\Laravel\put;
use function Pest\Laravel\delete;
use function Pest\Laravel\withoutMiddleware;

beforeEach(function () {
    User::factory()
        ->has(
            Tab::factory()
                ->count(1)
                ->has(
                    Bookmark::factory()
                        ->count(2)
                )
        )->create();
});

it('can list tabs', function () {
    withoutMiddleware(CheckGoogleTokenValidity::class);
    get(route('tabs.getByUser', ['user' => User::first()->id]))
        ->assertStatus(200)
        ->assertJsonStructure([
            0 => [
                'id',
                'orderPosition',
                'title',
                'userId',
                'bookmarks' => [
                    0 => [
                        'id',
                        'isLink',
                        'orderPosition',
                        'parentBookmarkId',
                        'tabId',
                        'thumbnailUrl',
                        'title',
                        'url'
                    ]
                ]
            ],
        ]);
});

it('can store tabs', function () {
    withoutMiddleware(CheckGoogleTokenValidity::class);
    $response = post(route('tabs.store', ['user' => User::first()->id]), [
        'title' => "Youtube",
        'orderPosition' => 3,
    ]);
    $response->assertCreated();
    $this->assertDatabaseHas('tabs', [
        'title' => 'Youtube'
    ]);
});

it('can update tabs', function () {
    withoutMiddleware(CheckGoogleTokenValidity::class);
    $tabId = Tab::first()->id;
    $response = put(route('tabs.update', [
        'tab' => $tabId,
    ]), [
        'title' => 'Example',
    ]);
    $response->assertNoContent(200);
    $this->assertDatabaseHas('tabs', [
        'title' => 'Example'
    ]);
});

it('can delete tab', function () {
    withoutMiddleware(CheckGoogleTokenValidity::class);
    $tabId = Tab::first()->id;
    $response = delete(route('tabs.delete', ['tab' => $tabId]));
    $response->assertNoContent();
    $this->assertDatabaseMissing('tabs', [
        'id' => $tabId
    ]);
});

it('forbids to interact with tabs if not authenticated', function () {
    get(route('tabs.getByUser', ['user' => 1]))
        ->assertStatus(401);
    post(route('tabs.store', ['user' => 1]))
        ->assertStatus(401);
    put(route('tabs.update', ['tab' => 1]))
        ->assertStatus(401);
    delete(route('tabs.delete', ['tab' => 1]))
        ->assertStatus(401);
});
