<?php

use App\Http\Middleware\CheckGoogleTokenValidity;
use App\Models\Bookmark;
use App\Models\Tab;
use App\Models\User;
use function Pest\Laravel\post;
use function Pest\Laravel\put;
use function Pest\Laravel\delete;
use function Pest\Laravel\withoutMiddleware;

beforeEach(function () {
    User::factory()
        ->has(
            Tab::factory()
                ->count(1)
                ->has(
                    Bookmark::factory()
                        ->count(2)
                )
        )->create();
});

it('can store bookmark', function () {
    withoutMiddleware(CheckGoogleTokenValidity::class);
    $response = post(route('bookmarks.store'), [
        'title' => "Youtube",
        'url' => "https://www.youtube.com/",
        'isLink' => true,
        'orderPosition' => 3,
        'tabId' => 1,
        'parentBookmarkId' => null
    ]);
    $response->assertCreated();
});

it('can update bookmark', function () {
    withoutMiddleware(CheckGoogleTokenValidity::class);
    $bookmarkId = 1;
    $response = put(route('bookmarks.update', [
        'bookmark' => $bookmarkId,
    ]), [
        'title' => 'Example',
        'url' => 'http://example.com/',
        'isLink' => true,
        'orderPosition' => 1,
        'thumbnailUrl' => null
    ]);
    $response->assertNoContent(200);
    $this->assertDatabaseHas('bookmarks', [
        'title' => 'Example'
    ]);
});


it('can delete bookmark', function () {
    withoutMiddleware(CheckGoogleTokenValidity::class);
    $bookmarkId = 1;
    $response = delete(route('bookmarks.delete', ['bookmark' => $bookmarkId]));
    $response->assertNoContent();
    $this->assertDatabaseMissing('bookmarks', [
        'id' => $bookmarkId
    ]);
});

it('forbids to interact with bookmarks if not authenticated', function () {
    post(route('bookmarks.store', ['user' => 1]))
        ->assertStatus(401);
    put(route('bookmarks.update', ['user' => 1, 'bookmark' => 1]))
        ->assertStatus(401);
    delete(route('bookmarks.delete', ['user' => 1, 'bookmark' => 1]))
        ->assertStatus(401);
});
