<?php

use App\Http\Middleware\CheckGoogleTokenValidity;
use App\Models\Bookmark;
use App\Models\Setting;
use App\Models\Tab;
use App\Models\User;
use function Pest\Laravel\put;
use function Pest\Laravel\withoutMiddleware;

beforeEach(function () {
    User::factory()
        ->has(
            Tab::factory()
                ->count(1)
                ->has(
                    Bookmark::factory()
                        ->count(2)
                )
        )->create();
});

it('can update tabs', function () {
    withoutMiddleware(CheckGoogleTokenValidity::class);
    $settingId = Setting::first()->id;
    $response = put(route('settings.update', [
        'setting' => $settingId,
    ]), [
        'backgroundImageUrl' => 'https://htmlcolorcodes.com/assets/images/html-color-codes-color-tutorials-hero.jpg',
        'bookmarkWidth' => 200,
        'showBookmarkTitle' => false,
        'openInNewTab' => true,
    ]);
    $response->assertNoContent(200);
    $this->assertDatabaseHas('settings', [
        'background_image_url' => 'https://htmlcolorcodes.com/assets/images/html-color-codes-color-tutorials-hero.jpg',
        'bookmark_width' => "200",
        'show_bookmark_title' => false,
        'open_in_new_tab' => true,
    ]);
});


it('forbids to interact with settings if not authenticated', function () {
    put(route('settings.update', ['setting' => 1]))
        ->assertStatus(401);
});
