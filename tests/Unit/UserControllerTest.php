<?php

use App\Http\Middleware\CheckGoogleTokenValidity;
use App\Models\User;
use function Pest\Laravel\get;
use function Pest\Laravel\withoutMiddleware;

it('forbids to list users if not authenticated', function () {
    get(route('users'))
        ->assertStatus(401);
});

it('can list users', function () {
    $users = User::factory()->count(3)->create()->map(function ($user) {
        return $user->only(['id', 'email', 'setting_id']);
    });
    withoutMiddleware(CheckGoogleTokenValidity::class);
    get(route('users'))
        ->assertStatus(200)
        ->assertJson($users->toArray())
        ->assertJsonStructure([
            '*' => ['id', 'email', 'setting_id']
        ]);
});
