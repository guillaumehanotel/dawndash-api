include .env
export
.PHONY: help test serve install dependencies generate-keys database refresh-database ownership cache-clear log-clear
.DEFAULT_GOAL= help

WEB_USER?=www-data

ifeq ($(APP_ENV), local)
	PREFIX = @docker-compose run
else ifeq ($(APP_ENV), production)
	PREFIX = @php
endif

ifeq ($(APP_ENV), local)
	PREFIX_COMPOSER = @docker-compose run
else ifeq ($(APP_ENV), production)
	PREFIX_COMPOSER = ''
endif

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
	@echo $(PREFIX)
serve:
	$(PREFIX) artisan serve --host=lvh.me

install:
	make dependencies
	make generate-keys
	make database
	make cache-clear

dependencies: composer.json package.json ## Installe les dépendances PHP & JS
	$(PREFIX_COMPOSER) composer install --no-dev --ignore-platform-reqs

generate-keys: ## Génération des clefs secrètes
	$(PREFIX) artisan key:generate --force

database: ## Migration et population de la base de donnée
	$(PREFIX) artisan migrate:refresh --force
	$(PREFIX) artisan db:seed --force

refresh-database:
	$(PREFIX) artisan migrate:refresh --seed

ownership: bootstrap/cache storage ## Application des permissions et des appartenances
	chown -R $(WEB_USER):$(WEB_USER) .*
	chmod -R a+rw storage bootstrap/cache

cache-clear:  ## Vide le cache
	$(PREFIX) artisan config:cache
	$(PREFIX) artisan cache:clear

log-clear: ## Vide les logs
	@echo '' > storage/logs/laravel.log

test: vendor ## Execute les tests unitaires
	$(PREFIX) artisan test
