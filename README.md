# Dawndash API

Lancer le docker-compose en daemon : 
```
dcupd
```

URL de l'API :
```
http://localhost:8000/api/
```

Lancer les tests :
```
dcr artisan test
OR
docker-compose run artisan test
OR
composer test
```

Vérifier le codestyle :
```
./vendor/bin/phpcs
OR
composer check-style
```



Kill le docker-compose : 
```
dcdn
```

## TODO

https://github.com/lorisleiva/laravel-deployer

## Laravel CLI Helper :

Reload config :
```
dcr artisan config:clear
dcr artisan config:cache
```

Reload Database:
```
dcr artisan migrate:fresh
```
