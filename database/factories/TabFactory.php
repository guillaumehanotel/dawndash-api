<?php

namespace Database\Factories;

use App\Models\Tab;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TabFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tab::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
            'order_position' => $this->faker->unique()->randomDigitNotNull,
//            'user_id' => User::factory()
        ];
    }
}
