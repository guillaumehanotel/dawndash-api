<?php

namespace Database\Factories;

use App\Models\Bookmark;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookmarkFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bookmark::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'is_link' => $this->faker->boolean,
            'order_position' => $this->faker->unique()->randomDigitNotNull,
            'url' => $this->faker->url,
            'thumbnail_url' => null,
            'title' => $this->faker->domainName,
            'parent_bookmark_id' => null,
//            'tab_id' => '',
        ];
    }
}
