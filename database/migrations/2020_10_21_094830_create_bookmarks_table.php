<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmarks', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->boolean('is_link');
            $table->integer('order_position');
            $table->string('url')->nullable();
            $table->string('thumbnail_url')->nullable();
            $table->string('title');

            $table->foreignId('parent_bookmark_id')
                ->nullable()
                ->constrained('bookmarks');

            $table->foreignId('tab_id')
                ->constrained()
                ->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmarks');
    }
}
