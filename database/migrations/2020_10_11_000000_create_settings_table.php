<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('background_image_url')->nullable()->default("https://storage.cloud.google.com/dawndash-273809/vivaldi-gradient-wallpaper.jpg");
            $table->integer('bookmark_width')->default(250);
            $table->boolean('show_bookmark_title')->default(true);
            $table->boolean('open_in_new_tab')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
