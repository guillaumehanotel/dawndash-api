<?php

use App\Http\Controllers\BookmarkController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TabController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** USERS */
Route::get('/me', [UserController::class, 'getOrCreateUser'])->name('users.getOrCreate');

Route::group(['prefix' => 'users'], function () {
    Route::get('/', [UserController::class, 'index'])->name('users');
});

/** TABS */
Route::group(['prefix' => 'users/{user}/tabs'], function () {
    Route::get('/', [TabController::class, 'getTabsByUser'])->name('tabs.getByUser');
    Route::post('/', [TabController::class, 'store'])->name('tabs.store');
});

Route::group(['prefix' => 'tabs'], function () {
    Route::put('/{tab}', [TabController::class, 'update'])->name('tabs.update');
    Route::delete('/{tab}', [TabController::class, 'delete'])->name('tabs.delete');
});

/** BOOKMARKS */
Route::group(['prefix' => 'bookmarks'], function () {
    Route::post('/', [BookmarkController::class, 'store'])->name('bookmarks.store');
    Route::put('/{bookmark}', [BookmarkController::class, 'update'])
        ->name('bookmarks.update')
        ->where('bookmark', '[0-9]+');
    Route::patch('/bulk', [BookmarkController::class, 'bulkUpdate'])->name('bookmarks.bulk.update');
    Route::delete('/{bookmark}', [BookmarkController::class, 'delete'])->name('bookmarks.delete');
    Route::post('/import', [BookmarkController::class, 'importJsonChromeBookmarks'])->name('bookmarks.delete');
});

/** SETTINGS */
Route::group(['prefix' => 'settings'], function () {
    Route::put('/{setting}', [SettingController::class, 'update'])->name('settings.update');
});

Route::fallback(function () {
    abort(404);
});
